# PRJ101

# Description

This project is written as a teaching case study for use in a PRJ101, either in a seventh semester course, or possibly a course in GCIT.
The project module allows students to develop responsive, data-driven dynamic and interactive web applications using popular web technology.

# GROUP NUMBER 5

### Project Topic: Gyalpozhing Turf Reservation

## Purpose

Our system's goal is to enable customers to view the turf's schedule and reserve it by paying 50% of the whole cost in advance at a time that is convenient for them.

## Architecture

The Gyalpozhing turf reservation scheme improves the quality of life for football players. The user can check whether the time they want to play is free or reserved. They can then schedule a reservation for a time that is convenient for them. When the admin accepts or rejects their reservation, they will be informed by email.

<br>
On the other hand the admin will be notified by email when a user makes a reservation. He can then review the reservation form, check the journal number, and take appropriate action. i.e., if to accept or decline the reservation.

<br>
Our system will make life easier for both players and groundskeeper because it is straightforward and simple to use.

## Functionality

### User <br>

● Register, login <br>
Users have to register and then login to our system in order to reserve the turf. <br>
● Payment, cancel <br>
Users who reserve turf must make a half-advance payment, and they will have 30
minutes to cancel their reservation. <br>
● Schedules <br>
Users may always see whether the turf is reserved or available or pending. The
schedule will display in blue if it is available, red if it is reserved and yellow if it is
pending. <br>
● Profile <br>
Users personal information will be displayed in this section such as name, email id and
phone number. <br>
● Notification <br>
The user will be notified about the ground reservation upon confirmation by the admin
through Email. <br>

### Admin <br>

● Register, login <br>
First, the admin must register for our system before logging in to use it. <br>
● Perform CRUD operation <br>
The administrator can see the list of reservations and, after verifying the payment
information, must approve the user's reservation.

## Team Profile

Project Guide: Mrs. Pema Wangmo
<br>
<img src="images/Pema.jpg" width="250" height="250" />
<br>
Project Leader and Backend Developer Mr. Tandin Wangchuk
<br>
<img src="images/Tandin.JPG" width="250" height="220" />
<br>
Frontend Developer Mr. Tshering
<br>
<img src="images/Tshering.JPG" width="250" height="220" />
<br>
Frontend Developer Mr. Damber Signh Subedi
<br>
<img src="images/Damber.JPG" width="250" height="220" />
<br>
UI/UX Design Mrs. Dechen Pelden
<br>
<img src="images/Dechen.JPG" width="250" height="220" />
<br>

Poster
<br>
<img src="images/GTRposter.jpeg" width="300" height="400" />
<br>

# Links

Watch Promo-Video
<a href="https://www.youtube.com/watch?v=WRc-laLxX5o" >Click here to Watch </a>
<br>
Live Host
<a href="https://gyalpozhing-turf-reservation.herokuapp.com/" >https://gyalpozhing-turf-reservation.herokuapp.com </a>
